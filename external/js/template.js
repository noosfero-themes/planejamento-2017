function montaTemplate (tipoGrafico) {

    var template1 = [
        '<div class="velocimeter">',
            '<div id="container1_'+tipoGrafico+'" class="container1"></div>',
            '<div id="container2_'+tipoGrafico+'" class="container2"></div>',
            '<div id="'+tipoGrafico+'_fundo_left" class="fundo_left">',
                '<svg>',
                    '<path id="'+tipoGrafico+'_trail_left" fill="none" stroke="'+czMd+'" stroke-width="26" />',
                '</svg>',
            '</div>',
            '<div id="'+tipoGrafico+'_fundo_right" class="fundo_right">',
                '<svg>',
                    '<path id="'+tipoGrafico+'_trail_right" fill="none" stroke="'+czMd+'" stroke-width="26" />',
                '</svg>',
            '</div>',
            '<i class="fa fa-metas periodo" id="'+tipoGrafico+'_point-meta-periodo"></i>',
            '<i class="fa fa-metas" id="'+tipoGrafico+'_point-meta-anual"></i>',
            '<div id="'+tipoGrafico+'_linha_anual" class="linha_anual">',
                '<svg>',
                    '<line id="'+tipoGrafico+'_trail_anual"  x1="0" y1="0" x2="0" y2="0" stroke="#fff" stroke-width="1.5" />',
                '</svg>',
            '</div>',
            '<div id="'+tipoGrafico+'_legenda-metas" class="legenda-metas">',
                '<div> <i class="fa fa-metas periodo"></i>Meta do Período',
                '<div id="'+tipoGrafico+'_meta-periodo" class="meta-periodo"></div>',
                '</div>',
                '<div><i class="fa fa-metas"></i>Meta Anual',
                '<div id="'+tipoGrafico+'_meta-anual" class="meta-anual"></div></div>',
                '<div>Realizado no Período</div>',
            '</div>',
            '<i id="'+tipoGrafico+'_happy" class="fa fa-smile-o icon-white detail-happy"></i>',
            '<i id="'+tipoGrafico+'_sad" class="fa fa-frown-o icon-white detail-sad"></i>',
        '</div>',
    ].join("\n");

    var template2 = [
        '<div class="velocimeter">',
            '<div id="container1_'+tipoGrafico+'" class="container1"></div>',
            '<div id="container2_'+tipoGrafico+'" class="container2"></div>',
            '<div id="'+tipoGrafico+'_fundo_left" class="fundo_left">',
                '<svg>',
                    '<path id="'+tipoGrafico+'_trail_left" fill="none" stroke="'+czMd+'" stroke-width="26" />',
                '</svg>',
            '</div>',
            '<div id="'+tipoGrafico+'_fundo_right" class="fundo_right">',
                '<svg>',
                    '<path id="'+tipoGrafico+'_trail_right" fill="none" stroke="'+czMd+'" stroke-width="26" />',
                '</svg>',
            '</div>',
            '<i class="fa fa-metas anual" id="'+tipoGrafico+'_point-meta-periodo"></i>',
            '<div id="'+tipoGrafico+'_legenda-metas" class="legenda-metas">',
                '<div> <i class="fa fa-metas anual"></i>Meta do Período/Anual',
                    '<div id="'+tipoGrafico+'_meta-periodo" class="meta-periodo"></div>',
                '</div>',
                '<div class="single">Realizado no Período</div>',
            '</div>',
            '<i id="'+tipoGrafico+'_happy" class="fa fa-smile-o icon-white detail-happy"></i>',
            '<i id="'+tipoGrafico+'_sad" class="fa fa-frown-o icon-white detail-sad"></i>',
        '</div>',
    ].join("\n");


    switch (tipoGrafico) {
        case 0: return template1; break;
        case 1: return template1; break;
        case 2: return template2; break;
    }

}
