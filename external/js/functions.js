//cores
var vmCl = "#FFE5E5"; //vermelhoClaro
var vmMd = "#EF4534"; //vermelhoMedio
var vmEs = "#B22828"; //vermelhoEscuro

var vdCl = "#E5F1D2"; //verdeClaro
var vdMd = "#8CC14F"; //verdeMedio
var vdEs = "#7BA346"; //verdeEscuro

var czMd = "#aaa196"; //cinzaMedio
var czVm = "#E2C5C5"; //cinzaVermelho
var czVd = "#C8D6C8"; //cinzaVerde


function formatCurrency(num) {
    num = num.toString().replace(/|\,/g, '');
    if (isNaN(num))
    {
        num = "0";
    }

    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.5);
    cents = num % 100;
    num = Math.floor(num / 100).toString();

    if (cents < 10)
    {
        cents = "0" + cents;
    }
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
    {
        num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
    }

    return (((sign) ? '' : '-') + '' + num + ',' + cents);
}

function formatPercentual(num, metaPeriodo, metaAnual) {
    num = num.toString().replace(/|\,/g, '');
    var formatado = "";

    if (isNaN(num))
    {
        num = "0";
    }

    if (num > metaPeriodo) formatado = "+";
    num = num-50;// - 0.5;
    console.log(num);
    formatado += num.toFixed(2).toString() // arrendonda
    formatado = formatado.replace(".", ",") + "%";
        

    return formatado;
}

function plotTrail(centerX, centerY, radius, intersec) {
    var angleInRadians = (intersec-180) * Math.PI / 180.0;

    return {
        x: centerX + (radius * Math.cos(angleInRadians)),
        y: centerY + (radius * Math.sin(angleInRadians))
    };
}

function formatMilhoes(num) {
    num = num.toString().replace(/|\,/g, '');
    if (isNaN(num))
    {
        num = "0";
    }

    var formatado = formatCurrency(num);
    var milhoes = Math.pow(10, 6);
    var bilhoes = Math.pow(10, 9);

    if (num >= milhoes && num < bilhoes ) {  //milhoes
        num = num/milhoes;
        //formatado = num.toFixed(2) // arrendonda
        formatado = num.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]; //trunca 
        formatado += " mi";
        formatado =formatado.replace(".", ",");
        
    }
    else if (num >= bilhoes ) {  //bilhoes
        num = num/bilhoes;
        //formatado = num.toFixed(2); // arredonda
        formatado = num.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]; //trunca
        formatado += " bi";
        formatado = formatado.replace(".", ",");
       
    }

    return formatado;
}

function describeArc(x, y, radius, startAngle, endAngle){

        var start = plotTrail(x, y, radius, endAngle);
        var end = plotTrail(x, y, radius, startAngle);

        var largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";

        var d = [
            "M", start.x, start.y, 
            "A", radius, radius, 0, largeArcFlag, 0, end.x, end.y
        ].join(" ");

        return d;       
}

function pointMeta(tipoGrafico, x, y , raio, intersec, plotAnual, metaPeriodo, metaAnual){
    pointMP = document.getElementById(tipoGrafico+"_point-meta-periodo");
    pointMA = document.getElementById(tipoGrafico+"_point-meta-anual");
    textMP = document.getElementById(tipoGrafico+"_meta-periodo");
    textMA = document.getElementById(tipoGrafico+"_meta-anual");
    trailAnual = document.getElementById(tipoGrafico+"_trail_anual");

    pointMP.style.top = plotTrail(x, y, x, intersec).y + "px";
    pointMP.style.left = plotTrail(x,  y, x, intersec).x + "px";
    
    pointMA.style.top = plotTrail(x, y, x, plotAnual).y + "px";
    pointMA.style.left = plotTrail(x,  y, x, plotAnual).x + "px";

    textMP.innerHTML = formatMilhoes(metaPeriodo); 
    textMA.innerHTML = formatMilhoes(metaAnual);
    
    trailAnual.setAttribute('x1', x);
    trailAnual.setAttribute('y1', y);
    trailAnual.setAttribute('x2', plotTrail(x,  y, x, plotAnual).x);
    trailAnual.setAttribute('y2', plotTrail(x, y, x, plotAnual).y);
}

function pointMargen(tipoGrafico, x, y , raio, intersec, plotAnual, metaPeriodo, metaAnual){
    pointMP = document.getElementById(tipoGrafico+"_point-meta-periodo");
    textMP = document.getElementById(tipoGrafico+"_meta-periodo");
    pointMP.style.top = plotTrail(x, y, x, intersec).y + "px";
    pointMP.style.left = plotTrail(x,  y, x, intersec).x + "px";
    textMP.innerHTML = "0"; 
}




function plotMargem(container, realizado) {            

    // Passar para a função plotMargem
    // 1 - id da div para inserior o grafico
    // 2 - Tipo de Gráfico (1 = margem)
    // 3 - Realizado (valor entre -50 e 50)
       
    //constantes de ajuste de escala
    var metaPeriodo = 50;
    var metaAnual = 100;
    var offSet = 0; // distancia em graus da meta metaAnual para o limite do grafico
    var escala = (180 - offSet)/ 180; //ajuste da escala de valor baseada no limite
    //var plotRealizado = (realizado + 0.5).toFixed(2) * 100 // Conversão do valor realizado numa escala positiva de 0 a 100
    var plotRealizado = realizado + 50; //).toFixed(2) // Conversão do valor realizado numa escala positiva de 0 a 100
    console.log ("realizado: " + realizado);
    var plotValor = plotRealizado / metaAnual; //ponto do valor Acumulado no gráfico 
    console.log ("plotValor: " + plotValor);
    var plotMeta = metaPeriodo / metaAnual; // proporção entre as metas
    console.log ("plotMeta: " + plotMeta);
    var plotAnual = (180 - offSet); // graus até a meta anual
    tipoGrafico = 2;
    
    animaGraph (container, metaPeriodo, metaAnual, offSet, escala, plotValor, plotAnual, tipoGrafico );

}

function plotGraph(container, tipoGrafico, metaPeriodo, metaAnual, realizadoPeriodo) {            

    // Passar para a função plotGraph:
    // 1 - id da div para inserior o grafico
    // 2 - Tipo de Gráfico (0 = faturometro, 1 = despesometro)
    // 3 - Meta Acumulada do Período
    // 4 - Meta Anual
    // 5 - Valor Realizado do Período 
    
    //constantes de ajuste de escala
    var offSet = 15; // distancia em graus da meta metaAnual para o limite do grafico
    var escala = (180 - offSet)/ 180; //ajuste da escala de valor baseada no limite
    var plotValor = realizadoPeriodo / metaAnual; //ponto do valor Acumulado no gráfico 
    var plotMeta = metaPeriodo / metaAnual; // proporção entre as metas
    var plotAnual = (180 - offSet); // graus até a meta anual
    
    animaGraph (container, metaPeriodo, metaAnual, offSet, escala, plotValor, plotAnual, tipoGrafico );
    

}

function  animaGraph (container, metaPeriodo, metaAnual, offSet, escala, plotValor, plotAnual, tipoGrafico ){

    // constantes de desenho dos graficos
    var x = 86;
    var y = x + 8;
    var raio = x - 12;
    var intersec = metaPeriodo / metaAnual * plotAnual; // graus até a meta periodo
    var animeTime = 4000;  //Tempo metaAnual da animação
    //console.info(animeTime);

    //monta template 
    $("#"+container).append(montaTemplate(tipoGrafico));
    var progress1 = "#container1_"+tipoGrafico;
    var progress2 = "#container2_"+tipoGrafico;;

    //elementos animados
    document.getElementById(tipoGrafico+"_happy").style.display = tipoGrafico == 1 ? "block" : "none";
    document.getElementById(tipoGrafico+"_sad").style.display = tipoGrafico == 1 ? "none" : "block" ;
    document.getElementById(tipoGrafico+"_trail_left").setAttribute("stroke", tipoGrafico == 1 ? czVd : czVm );
    document.getElementById(tipoGrafico+"_fundo_left").style.zIndex = "1000";
   
    
    // progressbar.js@1.0.0 version is used
    // Docs: http://progressbarjs.readthedocs.org/en/1.0.0/
    var bar = new ProgressBar.SemiCircle( progress1, {
            strokeWidth: 15,
            color: vmMd,
            trailColor: 'transparent', 
            trailWidth: 15,
            easing: 'easeInOut',
            duration: animeTime,
            svgStyle: 'none',
            text: {
                value: '',
                alignToBottom: false
            },
            from: {color: tipoGrafico == 1 ? vdEs : vmMd},
            to: {color: tipoGrafico == 1 ? vmMd : vdEs},
            // Set default step function for all animate calls
            step: (state, bar) => {
                //bar.path.setAttribute('stroke', state.color);
                                
                if (tipoGrafico != 2) value = Math.round(bar.value() * metaAnual );
                else var value = bar.value() * metaAnual;
                
                if (value === 0) {
                    bar.setText('');
                } else {
                    if (tipoGrafico != 2) bar.setText( Math.round(value/escala));
                    else bar.setText(value/escala);
                    
                    if (Math.round(value/escala) < metaPeriodo) { 
                        bar.path.setAttribute('stroke', tipoGrafico == 1 ? vdEs : vmMd );
                        bar.text.style.color =  tipoGrafico == 1 ? vdEs : vmMd ;
                           
                    }
                    else {
                        bar.path.setAttribute('stroke', tipoGrafico == 1 ? vmEs : vdEs );
                        bar.text.style.color = tipoGrafico == 1 ? vmMd : vdEs;
                                               
                        document.getElementById(tipoGrafico+"_happy").style.display = tipoGrafico == 1 ? "none" : "block";
                        document.getElementById(tipoGrafico+"_sad").style.display = tipoGrafico == 1 ? "block" : "none";
                        document.getElementById(tipoGrafico+"_trail_left").setAttribute("stroke", tipoGrafico == 1 ? vmMd : vdMd );
                        document.getElementById(tipoGrafico+"_fundo_left").style.zIndex = "1003";
                    }
                }

               //bar.setText(formatCurrency(bar.text.innerHTML));
                if (tipoGrafico != 2) bar.setText(formatMilhoes(bar.text.innerHTML, metaPeriodo, metaAnual));
                else bar.setText(formatPercentual(bar.text.innerHTML, metaPeriodo, metaAnual));
            }
        });
    
        var bar2 = new ProgressBar.SemiCircle(progress2, {
            strokeWidth: 50,
            color: vmMd,
            trailColor: 'transparent',
            trailWidth: 0,
            easing: 'easeInOut',
            duration: animeTime,
            svgStyle: null,
            text: {
                value: '',
                alignToBottom: false
            },
            from: {color: tipoGrafico == 1 ? vdCl : vmCl},
            to: {color: tipoGrafico == 1 ? vmCl :vdCl},
            // Set default step function for all animate calls
            step: (state, bar2) => {
                //bar2.path.setAttribute('stroke', state.color);
                var value = Math.round(bar2.value() * metaAnual);
                //correção do valor da c    pointMargen(tipoGrafico, x, y , raio, intersec, plotAnual, metaPeriodo, metaAnual);or na escala
                if (Math.round(value/escala) < metaPeriodo) { 
                        bar2.path.setAttribute('stroke', tipoGrafico == 1 ? vdCl : vmCl);
                    }
                    else {
                        bar2.path.setAttribute('stroke', tipoGrafico == 1 ? vmCl :vdCl);
                }           
            }
        });

     bar.animate(plotValor*escala);  // Number from 0.0 to 1.0
     bar2.animate(plotValor*escala);
    
    //Desenha o fundo/trailLeft da meta
    document.getElementById(tipoGrafico+"_trail_left").setAttribute("d", describeArc(x, y, raio, 0, intersec));
    document.getElementById(tipoGrafico+"_trail_right").setAttribute("d", describeArc(x, y, raio, intersec, 180));
    if (tipoGrafico != 2) pointMeta(tipoGrafico, x, y , raio, intersec, plotAnual, metaPeriodo, metaAnual);
    else   pointMargen(tipoGrafico, x, y , raio, intersec, plotAnual, metaPeriodo, metaAnual);
    

}



